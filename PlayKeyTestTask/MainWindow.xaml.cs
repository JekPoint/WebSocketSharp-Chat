﻿using System;
using System.Data;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using WebSocketSharp;
using WebSocketSharp.Server;


namespace PlayKeyTestTask
{
    public partial class MainWindow
    {
        private WebSocket _ws;
        private WebSocketServer _wssv;
        private string _history;

        readonly Timer _connertionWsTimer = new Timer(1000);
        public MainWindow()
        {
            InitializeComponent();
            _connertionWsTimer.Elapsed += OnTimedEvent;
            _connertionWsTimer.AutoReset = true;
            _connertionWsTimer.Enabled = true;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            if (_ws.ReadyState != WebSocketState.Open)
            {
                _ws.Connect();
                if (_ws.ReadyState == WebSocketState.Open)
                {
                    Dispatcher.Invoke(() => StartingServerButton.IsEnabled = false);
                }
            }
        }
               
        private void SendMessageButton_Click(object senderButton, RoutedEventArgs eButton)
        {
            SendMessageToServer();
        }

        private void StartingServerButton_Click(object sender, RoutedEventArgs e)
        {
            _wssv = new WebSocketServer($"ws://{ServerIpTextBox.Text}:4649");
            _wssv.AddWebSocketService("/ChatMessage", () => new ChatMessage(_history));
            try
            {
                _wssv.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ResiveMessageFromServer(MessageEventArgs msg)
        {
            if (msg.IsText)
            {
                _history = _history + msg.Data;
                Dispatcher.Invoke(() => HistoryTextBox.Text = _history);
            }
            if (msg.IsBinary)
            {
                string result = System.Text.Encoding.UTF8.GetString(msg.RawData);
                MessageBox.Show(result);
            }
        }

        private void MessageTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SendMessageToServer();
            }
        }

        private void SendMessageToServer()
        {
            _ws.Send(MessageTextBox.Text);
            MessageTextBox.Text = "";
        }

        private void ClientConnectToServer(string address)
        {
            _ws = new WebSocket($"ws://{address}:4649/ChatMessage");
            _ws.OnMessage += (sender, e) => ResiveMessageFromServer(e);
            _ws.OnOpen += (sender, e) =>
            {
                _ws.Send(new byte[] { 0x00 });
                Dispatcher.Invoke(() => StartingServerButton.IsEnabled = false);
            };
            _ws.OnClose += (sender, e) =>
            {
                Dispatcher.Invoke(() => StartingServerButton.IsEnabled = true);
            };
        }

        private void GetLogButton_Click(object sender, RoutedEventArgs e)
        {
            _ws.Send(new byte[] { 0x01 });
        }

        private void ServerIpTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                _ws.Close();
            }
            catch(Exception ex)
            { }
            ClientConnectToServer(ServerIpTextBox.Text);
        }

        private void Window_Loaded(object senderForm, RoutedEventArgs eForm)
        {
            ClientConnectToServer(ServerIpTextBox.Text);
        }

    }
    public class ChatMessage : WebSocketBehavior
    {
        private DataTable _historyTable;
        private string _history;
        protected override void OnMessage(MessageEventArgs e)
        {
            if (e.IsText)
            {
                Sessions.Broadcast(DateTime.Now.ToLongTimeString() + ": " + e.Data + "\r\n");
                _historyTable.Rows.Add(e.Data);
                var dataView = new DataView(_historyTable)
                {
                    Sort = "Message ASC"
                };
                try
                {
                    FileStream history = new FileStream("History.txt", FileMode.OpenOrCreate);

                    using (StreamWriter sw = new StreamWriter(history))
                    {
                        foreach (DataRowView row in dataView)
                        {
                            sw.WriteLine(row[0]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "/r/n" + ex.StackTrace);
                }

            }
            if (e.IsBinary)
            {
                if (e.RawData[0] == 0x00)
                {
                    Send(_history);
                }
                if (e.RawData[0] == 0x01)
                {
                    Send(new FileInfo("History.txt"));
                }
            }
        }

        protected override void OnOpen()
        {
            _historyTable = new DataTable();
            _historyTable.Columns.Add("Message");
            try
            {
                FileStream history = new FileStream("History.txt", FileMode.OpenOrCreate);

                using (StreamReader sr = new StreamReader(history))
                {
                    while (sr.Peek() >= 0)
                    {
                        _historyTable.Rows.Add(sr.ReadLine());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "/r/n" + ex.StackTrace);
            }
        }

        public ChatMessage(string suffix)
        {
            _history = suffix;
        }
    }
}
